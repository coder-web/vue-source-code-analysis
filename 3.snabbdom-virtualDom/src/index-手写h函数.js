import h from './snabbdom/h'

// let myVnode =  h('a', {props :{href: 'https://www.baidu.com'}}, '百度')
let myVnode =  h('a', {props :{href: 'https://www.baidu.com'}}, [
  h('div', {}, '你好'),
  h('p', {}, [
    h('ul',{}, [
      h('li', {}, '苹果')
    ])
  ])
])
console.log(myVnode)