import {
  init,
  classModule,
  propsModule,
  styleModule,
  eventListenersModule,
  h,
} from "snabbdom";

// 创建path函数
const patch = init([classModule, propsModule, styleModule, eventListenersModule])

// 创建虚拟节点
let myVNode1 = h('ul', {}, [
  h('li',{}, 'A'),
  h('li',{}, 'B'),
  h('li',{}, 'C'),
  h('li',{}, 'D')
])
console.log(myVNode1)
let myVNode2 = h('ul', {}, [
  h('li',{}, 'A'),
  h('li',{}, 'B'),
  h('li',{}, 'C'),
  h('li',{}, 'D'),
  h('li',{}, 'E')
])

// 让虚拟dim上树
const container = document.getElementById('container')
patch(container, myVNode1)

const btn = document.querySelector('button')
btn.addEventListener('click', function () {
  patch(myVNode1, myVNode2)
})