import {
  init,
  classModule,
  propsModule,
  styleModule,
  eventListenersModule,
  h,
} from "snabbdom";

// 创建path函数
const patch = init([classModule, propsModule, styleModule, eventListenersModule])

// 创建虚拟节点
let myVNode1 = h('a', {props :{href: 'https://www.baidu.com'}}, '百度')
console.log(myVNode1)

// 让虚拟dim上树
const container = document.getElementById('container')
patch(container, myVNode1)