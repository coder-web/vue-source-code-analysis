import patch from './snabbdom/patch'
import h from './snabbdom/h'

// 创建虚拟节点
/* let myVnode1 =  h('a', {props :{href: 'https://www.baidu.com'}}, [
  h('div', {}, '你好'),
  h('p', {}, [
    h('ul',{}, [
      h('li', {}, '苹果')
    ])
  ])
])
console.log(myVnode1) */
let myVnode1 = h('div', {key: 'vnode1'}, [
  h('p', {key: 'A'}, 'A'),
  h('p', {key: 'C'}, 'C'),
  h('p', {key: 'Q'}, 'q'),
  h('p', {key: 'D'}, 'D'),
  h('p', {key: 'LILI'}, 'LILI')
])

// 让虚拟dim上树
const container = document.getElementById('container')
patch(container, myVnode1)

const myVnode2 = h('div', {key: 'vnode1'}, [
  h('p', {key: 'A'}, 'A'),
  h('p', {key: 'B'}, 'B'),
  h('p', {key: 'C'}, 'C'),
  h('p', {key: 'D'}, 'D'),
  h('p', {key: 'E'}, 'E')
])
const changeBtn = document.querySelector('.changeBtn')
changeBtn.onclick = function () {
  patch(myVnode1, myVnode2)
}