import isSameVnode from "./isSameVnode"
import patchVnode from "./patchVnode"
import createElement from "./createElement"
import patch from "./patch"

export default function (parentElm, oldCh, newCh) {
  let oldStartIdx = 0// 旧前索引
  let newStartIdx = 0// 新前索引
  let oldEndIdx = oldCh.length -1// 旧后索引
  let newEndIdx = newCh.length -1// 新后索引
  let oldStartVnode = oldCh[0]// 旧前节点
  let newStartVnode = newCh[0]// 新前节点
  let oldEndVnode = oldCh[oldEndIdx]// 旧后节点
  let newEndVnode = newCh[newEndIdx]// 新后节点

  let keyMap = {}

  // 开始循环判断
  while(oldStartIdx<=oldEndIdx && newStartIdx<=newEndIdx) {
    if (oldStartVnode === undefined) {
      oldStartVnode = oldCh[++oldStartIdx]
    } else if (oldEndVnode === undefined) {
      oldEndVnode = oldCh[--oldEndIdx]
    } else if(isSameVnode(oldStartVnode, newStartVnode)) {//新前和旧前
      // 将新前和旧前进行对比，结果就是把新节点上的文本或者子节点上到旧节点的dom上
      patchVnode(oldStartVnode, newStartVnode)
      // 新旧节点指针下移
      oldStartVnode = oldCh[++oldStartIdx]
      newStartVnode = newCh[++newStartIdx]
    } else if (isSameVnode(oldEndVnode, newEndVnode)) {// 新后和旧后
      // 将新后和旧后进行对比，结果就是把新节点上的文本或者子节点上到旧节点的dom上
      patchVnode(oldEndVnode, newEndVnode)
      oldEndVnode = oldCh[--oldEndIdx]
      newEndVnode = newCh[--newEndIdx]
      // 新前和旧前，新后和旧后，不需要一定dom的位置，只要把对应位置的dom内容替换掉就可以了
    } else if (isSameVnode(oldStartVnode, newEndVnode)) {// 新后和旧前
      // 将新后和旧前进行对比,将旧节点上的内容上到新节点的dom上
      patchVnode(oldStartVnode, newEndVnode)
      // 同时因为是对应的新后的位置，所以该dom应该是当前未处理的所有dom的后面，也就是当前未处理的dom的下一个兄弟节点的前面
      parentElm.insertBefore(oldStartVnode.elm, oldEndVnode.elm.nextSibling)
      oldStartVnode = oldCh[++oldStartIdx]
      newEndVnode = newCh[--newEndIdx]
    } else if (isSameVnode(oldEndVnode, newStartVnode)) {// 新前和旧后
      // 将新前和旧后进行对比,将旧节点上的内容上到新节点的dom上
      patch(oldEndVnode, newStartVnode)
      // 同时因为是对应的新前的位置，所以该dom应该是当前未处理的所有dom的钱面，也就是当前未处理的所有dom第一个的前面
      parentElm.insertBefore(oldEndVnode.elm, oldStartIdx.elm)
      oldEndVnode = oldCh[--oldEndIdx]
      newStartVnode = newCh[++newStartIdx]
    } else {
      // 四种都没有命中
      // 制作oldCh的keyMap
      for(let i=0,l=oldCh.length;i<l;i++) {
        let key = oldCh[i]?.key
        if(key !== undefined) {
          keyMap[key] = i
        }
      }
      const oldToIdx = keyMap[newStartVnode.key]
      if(oldToIdx !== undefined) {
        // 表示在oldCh中存在和当前newStartVnode一样的节点
        const oldTomoveVnode = oldCh[oldToIdx]
        //比对新旧节点
        patch(oldTomoveVnode, newStartVnode)
        // 插入已经比对处理过后的节点到所以未处理旧节点之前
        parentElm.insertBefore(oldTomoveVnode.elm, oldStartVnode.elm)
        // 将已经处理过的子节点设置为undefined
        oldCh[oldToIdx] = undefined
      } else {
        // 表示在oldCh中不存在和当前newStartVnode一样的节点
        console.log(newStartVnode)
        parentElm.insertBefore(createElement(newStartVnode), oldStartVnode.elm)
      }

      newStartVnode = newCh[++newStartIdx]
      console.log(newEndVnode)
    }
  }
  if(newStartIdx <= newEndIdx) {
    // 此时新节点还有剩余，需要把新节点插入到旧节点后面去
    const beforeDom = oldCh[oldEndIdx+1] === undefined ? null : oldCh[oldEndIdx+1].elm//将要插入的标杆
    // 就是当前旧节点的结束子节点的后面，如果当前结束子节点不存在，标杆就是null
    for(let i = newStartIdx;i<= newEndIdx;i++) {
      parentElm.insertBefore(createElement(newCh[i]), beforeDom)
    }
  } else if (oldStartIdx <= oldEndIdx) {
    // 此时旧的节点没有处理完，需要删除剩余的旧子节点
    for (let i= oldStartIdx;i<= oldEndIdx;i++) {
      if (oldCh[i] !== undefined) 
        parentElm.removeChild(oldCh[i].elm)
    }
  }
}