import vnode from './vnode'

// 编写一个低配版的h函数，这个函数必须接受3个参数，缺一不可
// 相当于它的重载功能较弱（没有）
// 也就是说，调用的时候形态必须是下面的三种之一
// h('div', {}, '文字')
// h('div', {}, '[]')
// h('div', {}, h())
export default function (sel, data, c) {
  // 检查参数的个数
  if (arguments.length !== 3) throw new Error('Sorry, h函数的参数必须是3个')
  // 检查参数的类型
  if (typeof c === 'string' || typeof c === 'number') {
    // 形态1，文本类型
    return vnode(sel,data,undefined,c, undefined)
  } else if (Array.isArray(c)) {
    let children = []
    // // h('div', {}, '[]')
    for (let i=0,l=c.length;i<l;i++) {
      if(!isVnode(c[i])) throw new Error('传入的数组参数中有项不是h函数的调用')
      children.push(c[i])
    }
    return vnode(sel, data, children, undefined, undefined)
  } else if (isVnode(c)) {
    return vnode(sel, data, [c], undefined, undefined)
  } else {
    throw new Error('传入的第三个参数类型不对')
  }
}

function isVnode (vnode) {
  return typeof vnode === 'object' && vnode.hasOwnProperty('sel')
}