import createElement from "./createElement"
import updateChildren from "./updateChildren"

export default function (oldVnode, newVnode) {
  // 判断是否是同一个Vnode
  if (newVnode === oldVnode) return
    // 判断newVnode是否有text属性
    if (newVnode.text !== undefined && (newVnode.children === undefined || newVnode.children.length === 0)) {
      if (newVnode.text !== oldVnode.text) {
        oldVnode.elm.innerText = newVnode.text 
      }
    } else {
      // 新Vnode没有text属性，在简化版中没有text一定有children
      // 判断老Vnode有没有children
      if (oldVnode.children !== undefined && oldVnode.children.length > 0) {
        // 有的话就要对新老children进行精细化比较
        updateChildren(oldVnode.elm, oldVnode.children, newVnode.children)
      } else {
        // 新的Vnode有children,旧的Vnode没有，把新的children追加到旧的Vnode上的elm上
        // 先把旧的Vnode上的text清空
        oldVnode.text = oldVnode.text ?? undefined
        oldVnode.elm.innerText = ''
        for (let i = 0,l = newVnode.children.length; i<l;i++) {
          const childDom = createElement(newVnode.children[i])
          oldVnode.elm.appendChild(childDom)
        }
      }
    }
}