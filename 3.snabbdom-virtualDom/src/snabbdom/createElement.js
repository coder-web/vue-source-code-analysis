export default function createElement (vNode) {
  let domNode = document.createElement(vNode.sel)
  vNode.elm = domNode
  if((vNode.text !== '' || vNode.text !== undefined) && (vNode.children === undefined || vNode.children.length === 0)) {
    // 该vnode是一个单纯的文本node
    domNode.innerText = vNode.text
  } else if (Array.isArray(vNode.children) && vNode.children.length > 0) {
    // 该vnode具有children属性
    for(let i = 0,l = vNode.children.length;i < l; i++) {
      let domItem = createElement(vNode.children[i])
      vNode.elm.appendChild(domItem)
    }
  }
  return domNode
}