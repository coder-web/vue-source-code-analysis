import vnode from './vnode'
import patchVnode from './patchVnode'
import isSameVnode from './isSameVnode'
import createElement from './createElement'

export default function (oldVnode, newVnode) {
  // 判断oldVnode 是否是dom
  if (oldVnode instanceof Element) {
    // 是dom的话就转化为Vnode
    oldVnode = vnode(oldVnode.tagName.toLocaleLowerCase(), {}, undefined, undefined, oldVnode)
  }
  if(isSameVnode(oldVnode, newVnode)) {
    // 是的话就进行精细化比较
    patchVnode(oldVnode, newVnode)
  } else {
    // 不是的话就暴力拆除
    // 创建新的dom节点
    let newDom = createElement(newVnode)
    // 移除旧的dom节点
    oldVnode.elm.parentNode.insertBefore(newDom, oldVnode.elm)
    oldVnode.elm.parentNode.removeChild(oldVnode.elm)
  }
}
