export default function (oldVnode, newVnode) {
  if(oldVnode.hasOwnProperty('key') && newVnode.hasOwnProperty('key')) {
    return oldVnode.sel === newVnode.sel && oldVnode?.key === newVnode?.key
  } else {
    return oldVnode === newVnode
  }
}