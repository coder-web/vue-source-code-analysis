const path = require('path');

module.exports = {
  // 入口
  entry: './src/index.js',
  // 出口
  output: {
    // publicPath 虚拟出口
    publicPath: 'virtual',
    // 打包出来的文件名
    filename: 'bundle.js'
  },
  devServer: {
    port: 8080,
    // 静态资源文件夹
    contentBase: 'www'
  }
};