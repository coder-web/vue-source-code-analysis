import parsePath from "./parsePath"
import parseArray from "./parseArray"
/**
 * 函数的功能是让tokens数组变为dom字符串 
 */

export default function renderTemplate (tokens, data) {
  console.log(tokens, data)
  let resultStr = ''
  for(let i=0,l=tokens.length;i<l;i++) {
    let token = tokens[i]
    if(token[0] === 'text') {
      resultStr += token[1]
    } else if (token[0] === 'name') {
      if (token === '.') {
        console.log(data)
        resultStr += data
      } else {
        resultStr += parsePath(token[1], data)
      }
    } else if(token[0] === '#') {
      resultStr += parseArray(token, data)
    }
  }
  return resultStr
}