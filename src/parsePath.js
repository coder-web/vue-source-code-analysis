
export default function parsePath (path, obj) {
  const bailRE = /[^\w.$]/
  if (bailRE.test(path)) return
  const sugments = path.split('.')
  for(let i=0,l=sugments.length;i<l;i++) {
    if(!obj) return
    obj = obj[sugments[i]]
  }
  return obj
}