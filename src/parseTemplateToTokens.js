import Scanner from "./Scanner";
import nestToekns from "./nestTokens";

/**
 * 将模板字符串变为tokens数组
 * 
 */ 
export default function parseTemplateToTokens (templateStr) {
  console.log(templateStr)
  const tokens = []
  // 创建扫描器扫描模板字符串
  const scanner = new Scanner(templateStr)
  let word
  while (!scanner.eos()) {
    // 收集开始标记前的字符
    word = scanner.scanUtil('{{')
    // 将标记前的字符推进tokens数组
    if (word !== '') tokens.push(['text', word])
    // 跳过当前标记字符 {{
    scanner.scan('{{')

    word = scanner.scanUtil('}}')
    if (word !== '') {
      if (word[0] === '#') {
        tokens.push(['#', word.substring(1)])
      } else if (word[0] === '/') {
        tokens.push(['/', word.substring(1)])
      } else {
        tokens.push(['name', word])
      }
    }
    scanner.scan('}}')
  }
  console.log(tokens, 222)
  return nestToekns(tokens)
}