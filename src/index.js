import parseTemplateToTokens from './parseTemplateToTokens'
import renderTemplate from './renderTemplate'

window.SGG_TemplateEngine = {
  render: function (str, data) {
    var tokens = parseTemplateToTokens(str)
    console.log(tokens, 444)
    let domStr = renderTemplate(tokens, data)
    console.log(domStr)
  }
}