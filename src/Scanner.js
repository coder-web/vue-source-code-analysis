export default class Scanner {
  constructor (str) {
    // 保存传进来的模板字符串
    this.templateStr = str
    // 默认的模板字符串的指针
    this.pos = 0
    // 模板字符串截取stopTag后的尾巴字符串，默认值和templateStr相同
    this.tail = str
  }
  // 功能就是路过指定内容，没有返回值
  scan (stopTag) {
    this.pos += stopTag.length
    this.tail = this.templateStr.substring(this.pos)
  }
  // 让指针进行扫描，直到遇见指定内容文字，并且能够返回结束之前路过的文字
  scanUtil (stopTag) {
    // 保存每次调用该函数时，指针的初始值
    let pos_backup = this.pos
    // 每次指针移动，判断尾巴字符串是否是以 stopTag 开头
    while (this.tail.indexOf(stopTag) !== 0 && !this.eos()) {
      // 不是以stopTag 开头的话，就将指针后移
      this.pos++
      // 后移指针的同时，将尾巴字符串也相应修改
      this.tail = this.templateStr.substr(this.pos)
    }
    // 将截取尾巴字符串后，指针经历过得字符返回
    return this.templateStr.substring(pos_backup, this.pos)
  }
  eos () {
    return this.pos >= this.templateStr.length
  }
}