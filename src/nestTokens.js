
export default function nestToekns (tokens) {
  /**
   * 折叠零散的tokens，将嵌套的模板字符串设置为嵌套数组
   * */ 
  // 需要返回的tokens数组
  const nestToekns = []

  // 栈结构，用来保存当前模板字符型里面的数组结构，嵌套的数组会压栈
  const tokenStack = []
  // 遍历传进来的tokens数组
  for(let i=0,l = tokens.length;i<l;i++) {
    // 保存当前遍历的token
    let token = tokens[i]
    switch(token[0]) {
      // 假如token第一项是'#'，表示开始进入字符串数组结构
      case '#':
        // ['#','app']
        // 设置当前token索引为2的项为 []
        token[2] = []//  ['#','app', []]
        // 将当前token推进保存遍历数组的栈结构
        tokenStack.push(token)
        break;
      case '/':
        // 假如token第一项是'/'，表示开始结束字符串数组结构
        // ['/','app']
        // 获取保存遍历数组的栈结构的栈顶，此时已经把当前数组里的所有数据都推进栈顶的数组里
        const tokenItem = tokenStack.pop()// ['/','app', [....]]
        // 判断当前栈结构是否为空
        if (tokenStack.length >= 1) {
          // 如果不为空的话，表示当前数组是嵌套内的数组，不能push进返回的tokens数组里，而是推进新的栈顶里
          tokenStack[tokenStack.length -1][2].push(tokenItem)
        } else {
          // 如果不为空的话，表示当前数组是结构，且外面没有嵌套数组，可以直接推进需要返回的tokens数组里
          nestToekns.push(tokenItem)
        }
        break;
      default:
        // 既不是 '#',也不是'/'，说明要么是数组外，要么是数组内部
        if(tokenStack.length === 0) {
          // 假如栈空了，说明当前所有数组已经结束，经历的token是可以直接推进返回的数组里的
          nestToekns.push(token)// ['text,'xxx']/['name', 'xxx']
        } else {
          // 栈没空的话，说明当前的token应该是数组里的，要把它推进当前栈顶里去
          tokenStack[tokenStack.length -1][2].push(token)
        }
        break;
    }
  }
  console.log(nestToekns)
  return nestToekns
}